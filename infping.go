// 
// Written by Tor Hveem copyright
// Improved by Ronan Bianic <rb@fkgm.fr>
// License: MIT
// Improved by Romanenko Denys <romanenkodenys@gmail.com>

package main

//-------------------------------------------------------------------------------------------------------
// Import section
import (  
    "github.com/influxdata/influxdb/client/v2"
    "github.com/pelletier/go-toml"
    "github.com/coreos/go-systemd/daemon" 
    "fmt"
    "flag"
    "log"
    "os"
    "bufio"
    "os/exec"
    "strings"
    "time"
    "strconv"
)

//-------------------------------------------------------------------------------------------------------
// Global Variables
var config *toml.Tree
var infdbcon client.Client
var agent_hostname string
var project string
var verbose bool
//-------------------------------------------------------------------------------------------------------
// Function - Connect to influx db
func dbConnect() {
    var err error
    hostname := config.Get("influxdb.host").(string)
    username := config.Get("influxdb.user").(string)
    password := config.Get("influxdb.pass").(string)

    infdbcon, err = client.NewHTTPClient(client.HTTPConfig{
        Addr:     hostname,
        Username: username,
        Password: password,
    })

   if err != nil {
        log.Fatal(err)
    }

    dur, ver, err := infdbcon.Ping(0)
    if err != nil {
        log.Fatal(err)
    }
    log.Printf("Connected to influxdb! %v, %s", dur, ver)

}
//-------------------------------------------------------------------------------------------------------
// Function - split by slash

func slashSplitter(c rune) bool {  
    return c == '/'
}
//-------------------------------------------------------------------------------------------------------
// Get fping data
func readPoints() {  

    args := []string{"-B 1", "-D", "-r0", "-O 0", "-Q 10", "-p 1000", "-l"}
    hosts := config.Get("hosts").([]*toml.Tree)
    
    ip_fqdn := make(map[string]string)
 
    for _, v := range hosts {
	fqdn, _ := v.Get("fqdn").(string)
	ip, _ := v.Get("ip").(string)
	ip_fqdn[ip] = fqdn;
	
	if verbose == true {
	    log.Printf("fqdn: %q",fqdn)
	}

        args = append(args, ip)
    }
    log.Printf("Going to ping the following hosts: %q", hosts)
    
    cmd := exec.Command("/usr/bin/fping", args...)
    stdout, err := cmd.StdoutPipe()
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

    stderr, err := cmd.StderrPipe()
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

    cmd.Start()
    if err != nil {
        fmt.Println(err)
    }

    buff := bufio.NewScanner(stderr)
    for buff.Scan() {
        text := buff.Text()
        fields := strings.Fields(text)
        // Ignore timestamp
        if len(fields) > 1 {
            host := ip_fqdn[fields[0]]
            data := fields[4]
            dataSplitted := strings.FieldsFunc(data, slashSplitter)
            // Remove ,
            dataSplitted[2] = strings.TrimRight(dataSplitted[2], "%,")
            sent, recv, lossp := dataSplitted[0], dataSplitted[1], dataSplitted[2]
            min, max, avg := "", "", ""
            // Ping times
            if len(fields) > 5 {
                times := fields[7]
                td := strings.FieldsFunc(times, slashSplitter)
                min, avg, max = td[0], td[1], td[2]
            }
	    if verbose == true {
        	log.Printf("Host:%s, loss: %s, min: %s, avg: %s, max: %s", host, lossp, min, avg, max)
	    }
            writePoints(host, sent, recv, lossp, min, avg, max)
        }
    }

    if verbose == true  {
	std := bufio.NewReader(stdout)
	line, err := std.ReadString('\n')

	if err != nil {
	    fmt.Println(err)
	}

	log.Printf("stdout:%s", line)
    }
}

//-------------------------------------------------------------------------------------------------------
// Write data to influx
func writePoints(fqdn string, sent string, recv string, lossp string, min string, avg string, max string) {  
// Create new Batchpoint for writing to db
    db := config.Get("influxdb.db").(string)
    batchpoint, err := client.NewBatchPoints(client.BatchPointsConfig{
	Database: db,
	Precision: "",
    })

    if err != nil {
	log.Fatal(err)
    }

// Process data from fping
    loss, _ := strconv.Atoi(lossp)
    fields := map[string]interface{}{}
    if min != "" && avg != "" && max != "" {
        min, _ := strconv.ParseFloat(min, 64)
        avg, _ := strconv.ParseFloat(avg, 64)
        max, _ := strconv.ParseFloat(max, 64)
        fields = map[string]interface{}{
                "loss": loss,
                "min": min,
                "avg": avg,
                "max": max,
        }
    } else {
        fields = map[string]interface{}{
                "loss": loss,
        }
    }

// Create tags
    tags:= map[string]string{
            "fqdn": fqdn,
	    "agent": agent_hostname,
	    "project": project,
        }

// Create point
    pt,err := client.NewPoint(
        "ping",
        tags,
	fields,
	time.Now(),
    )
    
    if err != nil {
	    log.Fatal(err)
	}

// Add point to batchpoint
	batchpoint.AddPoint(pt)

// Write batchpoint to influx database
    if err := infdbcon.Write(batchpoint); err != nil {
	log.Fatal(err)
    }

         daemon.SdNotify(false, "READY=1") // daemonize infping
}


//-------------------------------------------------------------------------------------------------------
// Main
func main() { 
    var err error
    var configfile string

// Get command line args
    flag.StringVar(&configfile, "config","infping.toml","Config file locaion")
    flag.BoolVar(&verbose, "verbose",false,"true/false")
    flag.Parse()
// Load config
    config, err = toml.LoadFile(configfile)  // You do specified explicitly the full path to config file
    if err != nil {
        fmt.Println("Error:", err.Error())
        os.Exit(1)
    }

    agent_hostname = config.Get("agent.hostname").(string)
    project = config.Get("global_tags.project").(string)

    log.Printf("Start influxping! agent=%s, project=%s", agent_hostname, project)

    dbConnect()
    readPoints()
}
