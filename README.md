# infping
Parse fping output, store result in influxdb

# Create influxdb database
CREATE USER ping  WITH PASSWORD [REDACTED]
CREATE DATABASE ping
GRANT ALL ON ping TO ping

# config.toml
[global_tags]
project = 
This is project name for selections in grafana dashboards

[agent]
hostname =
This is name of agent for selections in grafana dashboards

[influxdb]
This is section for access to influx Db

[[hosts]]
fqdn = 
Symbolic name of host to see in grafana dashboards
ip =
Actual ip of host to monitor.

Those schema is needed if host dont have resolved host name (routers, system hosts, etc)

Also included Grafana Dashboard Smokeping.json, and SystemD init file with auto restart service.

Usage: infping -config configfile.toml [-verbose]

Original post - https://wiki.fkgm.fr/ping-monitoring-with-infping/

